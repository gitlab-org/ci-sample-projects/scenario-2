# This is a forked project
Forked from https://gitlab.com/v_mishra/scenario-2

- Made a small change to the README on this project on a new branch, the merge request was created on the main project [MR1](https://gitlab.com/v_mishra/scenario-2/-/merge_requests/2). 3 pipelines started simultaneously. The job Test1 couldn't finish in a long time so I went ahead and cenceled it. A danger variant for `Merge` appeared with no explanation.
